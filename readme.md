# Continuos Integration - Final Project
## Define CI/CD Variables
![](captures/gitlab_ci_variables.png)
## Test Stage
```
test-jdk11:
  stage: test
  image: maven:3.6.3-jdk-11
  script:
    - mvn $MAVEN_CLI_OPTS clean org.jacoco:jacoco-maven-plugin:prepare-agent test jacoco:report
  artifacts:
    paths:
      - target/site/jacoco/jacoco.xml
  only:
    - master
```
## Sonarqube Stage
Add sonarqube to `pom.xml`
```
<properties>
  .
  .
  .
  <sonar.projectKey>project-final-maven</sonar.projectKey>
  <sonar.qualitygate.wait>true</sonar.qualitygate.wait>
</properties>

```
Add sonarqube stage to `.gitlab-ci.yml`
```
sonarqube-check:
  stage: sonarqube
  image: maven:3.6.3-jdk-11
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0" 
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script: 
    - mvn verify sonar:sonar
  allow_failure: true
  only:
    - master
```
### Sonarqube Result
![](captures/sonarqube_result.png)
## Artifactory Stage
Create configuration settings in jfrog artifactory `Set me up` option

![](captures/jfrog_set_me_up.png)

In this stage we'll use maven jfrog plugin

Set up `settings.xml`
```
<?xml version="1.0" encoding="UTF-8"?>
<settings xsi:schemaLocation="https://maven.apache.org/SETTINGS/1.2.0 https://maven.apache.org/xsd/settings-1.2.0.xsd" xmlns="https://maven.apache.org/SETTINGS/1.2.0"
    xmlns:xsi="https://www.w3.org/2001/XMLSchema-instance">
  <servers>
    <server>
      <username>franz.mejia@jala-foundation.org</username>
      <password>AP5tdzhJ9gtxWcUjGotrMfu2vUXppKDMzrMgXFY1rAW74kDYD</password>
      <id>central</id>
    </server>
    <server>
      <username>franz.mejia@jala-foundation.org</username>
      <password>AP5tdzhJ9gtxWcUjGotrMfu2vUXppKDMzrMgXFY1rAW74kDYD</password>
      <id>snapshots</id>
    </server>
  </servers>
  <profiles>
    <profile>
      <repositories>
        <repository>
          <snapshots>
            <enabled>false</enabled>
          </snapshots>
          <id>central</id>
          <name>default-maven-virtual</name>
          <url>https://fmejiajala.jfrog.io/artifactory/default-maven-virtual</url>
        </repository>
        <repository>
          <snapshots />
          <id>snapshots</id>
          <name>default-maven-virtual</name>
          <url>https://fmejiajala.jfrog.io/artifactory/default-maven-virtual</url>
        </repository>
      </repositories>
      <pluginRepositories>
        <pluginRepository>
          <snapshots>
            <enabled>false</enabled>
          </snapshots>
          <id>central</id>
          <name>default-maven-virtual</name>
          <url>https://fmejiajala.jfrog.io/artifactory/default-maven-virtual</url>
        </pluginRepository>
        <pluginRepository>
          <snapshots />
          <id>snapshots</id>
          <name>default-maven-virtual</name>
          <url>https://fmejiajala.jfrog.io/artifactory/default-maven-virtual</url>
        </pluginRepository>
      </pluginRepositories>
      <id>artifactory</id>
    </profile>
  </profiles>
  <activeProfiles>
    <activeProfile>artifactory</activeProfile>
  </activeProfiles>
</settings>
```
Set up `pom.xml` at the end of `project` tag
```
<project>
  .
  .
  .  
  <distributionManagement>
    <repository>
        <id>central</id>
        <name>a0pjeewxg7gnw-artifactory-primary-0-releases</name>
        <url>https://fmejiajala.jfrog.io/artifactory/jalasfot-project-maven-local</url>
    </repository>
    <snapshotRepository>
        <id>snapshots</id>
        <name>a0pjeewxg7gnw-artifactory-primary-0-snapshots</name>
        <url>https://fmejiajala.jfrog.io/artifactory/jalasfot-project-maven-local</url>
    </snapshotRepository>
  </distributionManagement>
</project>

```
`.gitlab-ci.yml`
```
jfrog:
  stage: deploy
  image: maven:3.6.3-jdk-11
  script: 
    - mvn $MAVEN_CLI_OPTS deploy -s settings.xml
  allow_failure: true
  only:
    - master
```
### Jfrog Artifactory Result

![](captures/jfrog_artifactory_jar.png)

## Snyk Stage
We used snyk maven plugin
```
<plugin>
  <groupId>io.snyk</groupId>
  <artifactId>snyk-maven-plugin</artifactId>
  <version>1.2.6</version>
  <executions>
    <execution>
      <id>snyk-test</id>
      <phase>test</phase>
      <goals>
        <goal>test</goal>
      </goals>
    </execution>
    <execution>
      <id>snyk-monitor</id>
      <phase>install</phase>
      <goals>
        <goal>monitor</goal>
      </goals>
    </execution>
  </executions>
  <configuration>
    <apiToken>${SNYK_TOKEN}</apiToken>
    <failOnSeverity>false</failOnSeverity>
    <org>fraj123</org>
    <args>
      <arg>--all-projects</arg>
    </args>
  </configuration>
</plugin>
```
Add config in `.gitlab-ci.yml`
```
snyk:
  stage: linting
  image: maven:3.6.3-jdk-11
  script:
    - mvn $MAVEN_CLI_OPTS snyk:test -DSNYK_TOKEN=$SNYK_TOKEN
    - mvn $MAVEN_CLI_OPTS snyk:monitor -DSNYK_TOKEN=$SNYK_TOKEN
  only:
    - master
```
### Snyk Result

https://snyk.io/org/fraj123/monitor/0a4a06f8-3552-4ed8-aaa1-219c5b4b7c8e

![](captures/snyk_result_project.png)
<br>
<br>
<br>
![](captures/snyk_result.png)

